package za.ac.sun.cs.mynetkey;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		
		TextView v = (TextView) findViewById(R.id.details_text);
		v.setText(InetkeyService.getString());
		
		InetkeyService.updateOn = this;
		
		Button back = (Button) findViewById(R.id.btnDisconnect);
		back.setText("Back");
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent back = new Intent(getApplicationContext(), LogonActivity.class);
				startActivity(back);
				finish();
			}
		});
		
	}
	
	
}
