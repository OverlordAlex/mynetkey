package za.ac.sun.cs.mynetkey;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;

public class InetkeyService extends Service {

	private static String url = "https://maties2.sun.ac.za/RTAD4-RPC3";
	private static String platform = "any";
	
	private static String username = "";
	private static String password = ""; 
	
	private static HashMap<String, Object> info;
	private static XMLRPCClient client;
	private static CallBackListener listener;
	
	static Timer time;
	
	private static HashMap<String, Object> values;
	public static boolean open;
	
	public static DetailsActivity updateOn;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		username = intent.getStringExtra("username");
		password = intent.getStringExtra("password");
		
		open = false;
		openInetkey();
		
		time = new Timer();
		time.schedule(new TimerTask() {          
	        @Override
	        public void run() {
	        	renewInetkey();
	        }

	    }, 0, 1000 * 60 * 10); // 1 second * every minute * 10 minutes
		
		return Service.START_REDELIVER_INTENT;
	}
	
	private static void openInetkey() {
		try {
			client = new XMLRPCClient(new URL(url));
		} catch (MalformedURLException e) {
			Log.e("inetkey", "Could not create XMLRPCClient");
		}
		
		listener = new CallBackListener();
		
		info = new HashMap<String, Object>();
		info.put("requser", username);
		info.put("reqpwd",  password);
		info.put("platform", platform);
	    
	    client.callAsync(listener, "rtad4inetkey_api_open", info);
	    open = true;
	}
	
	private void renewInetkey() { 
	    client.callAsync(listener, "rtad4inetkey_api_renew", info);
	    open = true;
	}
	
	public static void closeInetkey() {
	    client.callAsync(listener, "rtad4inetkey_api_close", info);
	    open = false;
	}
	
	private static class CallBackListener implements XMLRPCCallback {

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(long id, Object result) {
			values = (HashMap<String, Object>) result;
			
			
			
			
			if (updateOn != null) {
				
				
				
				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(updateOn.getApplicationContext());

			    ComponentName thisWidget = new ComponentName(updateOn.getApplicationContext(), mynetkeywidgitprovider.class);
			    
			    int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

			    for (int widgetId : allWidgetIds) {
					Intent intent;
					
					if (InetkeyService.open) {
						intent= new Intent(updateOn.getApplicationContext(), DetailsActivity.class);
					} else {
						intent= new Intent(updateOn.getApplicationContext(), LogonActivity.class);				
					}
					
					PendingIntent pendingIntent = PendingIntent.getActivity(updateOn.getApplicationContext(), 0, intent, 0);
					
					// Get the layout for the App Widget and attach an on-click listener
					// to the button
					RemoteViews views = new RemoteViews(updateOn.getApplicationContext().getPackageName(), R.layout.mynetkeywidgit);
					views.setOnClickPendingIntent(R.id.widgitbtn, pendingIntent);
					
					// To update a label
					Log.e("inetkey", InetkeyService.getString());
					Log.e("inetkey", ""+InetkeyService.open);
					views.setTextViewText(R.id.widgitlabel, InetkeyService.getString());
					
					if (open) {
						views.setTextViewText(R.id.widgitbtn, "Close");
					} else {
						views.setTextViewText(R.id.widgitbtn, "Open");
					}
					
					// Tell the AppWidgetManager to perform an update on the current app
					// widget
					appWidgetManager.updateAppWidget(widgetId, views);
			    }
				
				
				
				
				
				
				
				
				
				
				
				
				updateOn.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						final TextView v = (TextView) updateOn.findViewById(R.id.details_text);
						
						if (v != null) {
							v.setText(getString());
							
							Button back = (Button) updateOn.findViewById(R.id.btnDisconnect);
							back.setText("Log Out");
							back.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View arg0) {
									Intent back = new Intent(updateOn.getApplicationContext(), LogonActivity.class);
									closeInetkey();
									v.setText("Closed");
									updateOn.startActivity(back);
									updateOn.finish();
								}
							});
						}
						
						
					}
				});
			}
				
		}

		@Override
		public void onError(long id, XMLRPCException error) {
		}

		@Override
		public void onServerError(long id, XMLRPCServerException error) {
		}
		
	}

	@Override
	public void onDestroy() {
		time.cancel();
		closeInetkey();
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	public static String get(String key) {
		return (String) values.get(key);
	}
	
	public static String getString() {
		try {
			if (open) {
				StringBuilder b = new StringBuilder();
				b.append("State: " + (String) values.get("state"));
				b.append("\n");
				b.append("Month: " + (Double) values.get("monthusage")); // format??
				b.append("\n");
				b.append("Year: " + (Double) values.get("yearusage"));
				return b.toString();
			} else {
				return "Closed";
			}
		} catch (Exception e) {
			Log.e("inetkey", "exception: "+e.getMessage());
			return "Closed";
		}
	}
	
	public static boolean toggle() {
		if (open) {
			closeInetkey();
		} else {
			openInetkey();
		}
		return open;
	}

}
