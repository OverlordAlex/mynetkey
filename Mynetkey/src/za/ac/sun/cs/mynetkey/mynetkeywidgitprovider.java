package za.ac.sun.cs.mynetkey;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class mynetkeywidgitprovider extends AppWidgetProvider {

	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		final int N = appWidgetIds.length;
		
		// Perform this loop procedure for each App Widget that belongs to this
		// provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];
			
			Intent intent;
			
			if (InetkeyService.open) {
				intent= new Intent(context, DetailsActivity.class);
			} else {
				intent= new Intent(context, LogonActivity.class);				
			}
			
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
			
			// Get the layout for the App Widget and attach an on-click listener
			// to the button
			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.mynetkeywidgit);
			views.setOnClickPendingIntent(R.id.widgitbtn, pendingIntent);
			
			// To update a label
			views.setTextViewText(R.id.widgitlabel, InetkeyService.getString());
			
			// Tell the AppWidgetManager to perform an update on the current app
			// widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
			
		}
	}
	
	
}
