package za.ac.sun.cs.mynetkey;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LogonActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logon);
		
		Button logonButton = (Button) findViewById(R.id.logonBtn);
		
		logonButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String username = ((EditText) findViewById(R.id.usernameTxt)).getText().toString();
				String password = ((EditText) findViewById(R.id.passwordTxt)).getText().toString();
				
				Intent i = new Intent(LogonActivity.this, InetkeyService.class);
				i.putExtra("username", username);
				i.putExtra("password", password);
				startService(i); 
				
				Intent nextpage = new Intent(getApplicationContext(), DetailsActivity.class);
				startActivity(nextpage);
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logon, menu);
		return true;
	}

}
